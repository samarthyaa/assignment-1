package com.assignment.service;

import com.assignment.model.TicketRequest;
import com.assignment.model.TicketResponse;
import com.assignment.repository.ITicketRepository;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TicketServiceImplTest {

    private final AtomicInteger sequenceIncrementer = new AtomicInteger();

    @Autowired
    private ITicketRepository inMemoryTicketRepository;

    @Autowired
    private TicketServiceImpl service;

    @Test
    public void should_insert_new_record_on_create() {
        TicketRequest newTicket = buildNewTicketRequest("New Ticket");

        TicketResponse actualRes = service.createNewTicket(newTicket);

        assertNotNull(actualRes.getTicketId());
        assertEquals(newTicket.getDescription(), actualRes.getDescription());
    }

    @Test
    public void should_throw_error_empty_description_on_create() {
        TicketRequest newTicket = buildNewTicketRequest("");

        ResponseStatusException exception = Assertions.assertThrows(ResponseStatusException.class, () -> service.createNewTicket(newTicket));

        assertEquals("Ticket Description is mandatory", exception.getReason());
    }

    @Test
    public void should_update_status_description_on_update() {
        TicketRequest request = buildNewTicketRequest("New Ticket");
        TicketResponse newTicketRes = service.createNewTicket(request);
        // Update request
        request.setStatus("Closed");
        request.setDescription("Updated Ticket");

        TicketResponse expectedRes = service.updateTicket(request, newTicketRes.getTicketId());

        assertEquals("Closed", expectedRes.getStatus());
    }

    @Test
    public void should_not_update_empty_status_on_update() {
        TicketRequest request = buildNewTicketRequest("New Ticket");
        TicketResponse newTicketRes = service.createNewTicket(request);
        // Update request
        request.setStatus("");
        request.setDescription("Updated Ticket");

        TicketResponse expectedRes = service.updateTicket(request, newTicketRes.getTicketId());

        assertEquals("Open", expectedRes.getStatus());
    }

    @Test
    public void should_not_update_empty_description_on_update() {
        TicketRequest request = buildNewTicketRequest("New Ticket");
        TicketResponse newTicketRes = service.createNewTicket(request);
        // Update request
        request.setStatus("Closed");
        request.setDescription("");

        TicketResponse expectedRes = service.updateTicket(request, newTicketRes.getTicketId());
        assertEquals("New Ticket", expectedRes.getDescription());
    }

    @Test
    public void should_get_all_tickets() {
        int current = service.getAllTickets().size();
        addFourNewTickets();

        List<TicketResponse> allTickets = service.getAllTickets();

        assertEquals(current + 4, allTickets.size());
    }

    @Test
    public void should_get_ticket_by_id() {
        TicketRequest request = buildNewTicketRequest("New Ticket12");
        TicketResponse newTicketRes = service.createNewTicket(request);

        TicketResponse actualRes = service.getTicketsById(newTicketRes.getTicketId());

        assertEquals("New Ticket12", actualRes.getDescription());
    }

    @Test
    public void should_throw_error_for_non_existing_ticket() {
        ResponseStatusException exception = Assertions.assertThrows(ResponseStatusException.class, () -> service.getTicketsById(999L));

        assertEquals("Ticket not found", exception.getReason());
    }

    @Test
    public void should_do_successful_delete_ticket() {
        TicketRequest request = buildNewTicketRequest("Delete Ticket34");
        TicketResponse newTicketRes = service.createNewTicket(request);

        boolean isSuccess = service.deleteTicketsById(newTicketRes.getTicketId());

        assertEquals(true, isSuccess);
    }

    @Test
    public void should_fail_to_delete_ticket() {
        boolean isSuccess = service.deleteTicketsById(999L);

        assertEquals(false, isSuccess);
    }

    public TicketRequest buildNewTicketRequest(String description) {
        TicketRequest newTicketRequest = new TicketRequest();
        newTicketRequest.setDescription(description);
        return newTicketRequest;
    }

    public void addFourNewTickets() {
        TicketRequest newTicketRequest = buildNewTicketRequest("New Ticket");
        TicketRequest newTicketRequest2 = buildNewTicketRequest("New Ticket2");
        TicketRequest newTicketRequest3 = buildNewTicketRequest("New Ticket3");
        TicketRequest newTicketRequest4 = buildNewTicketRequest("New Ticket4");
        service.createNewTicket(newTicketRequest);
        service.createNewTicket(newTicketRequest2);
        service.createNewTicket(newTicketRequest3);
        service.createNewTicket(newTicketRequest4);
    }
}
