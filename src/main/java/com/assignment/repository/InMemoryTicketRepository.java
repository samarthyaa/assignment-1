package com.assignment.repository;

import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import com.assignment.model.Ticket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class InMemoryTicketRepository implements ITicketRepository {

    private final Map<Long, Ticket> ticketStore = new HashMap<>();
    private final AtomicInteger sequenceIncrementer = new AtomicInteger();

    Logger logger = LoggerFactory.getLogger(InMemoryTicketRepository.class);

    @Override
    public Ticket addTicket(Ticket ticket) {
        ticket.setTicketId(Long.valueOf(sequenceIncrementer.incrementAndGet()));
        ticket.setCreatedAt(Date.from(Instant.now()));
        ticket.setStatus("Open");
        ticketStore.put(ticket.getTicketId(), ticket);
        return ticket;
    }

    @Override
    public Ticket updateTicket(Ticket ticket, Long id) {
        ticketStore.put(id, ticket);
        return ticket;
    }

    @Override
    public List<Ticket> getAllTicket() {
        return ticketStore.values().stream().collect(Collectors.toList());
    }

    @Override
    public Ticket getTicketById(Long id) {
        return ticketStore.get(id);
    }

    @Override
    public boolean deleteTicketById(Long id) {
        boolean isSuccess = false;
        try {
            int storeSizeBefore = ticketStore.size();
            ticketStore.entrySet().removeIf(entry -> id.equals(entry.getKey()));
            int storeSizeAfter = ticketStore.size();
            if (storeSizeBefore > storeSizeAfter) {
                isSuccess = true;
            } else {
                throw new RuntimeException("Failed to delete ticket");
            }
        } catch (Exception e) {
            logger.error("Not able to delete ticket");
        }
        return isSuccess;
    }
}
